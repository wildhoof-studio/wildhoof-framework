<?php

declare(strict_types=1);

namespace Wildhoof\View;

use LogicException;
use RuntimeException;

use function array_merge;
use function array_replace;
use function extract;
use function file_exists;
use function ob_get_clean;
use function ob_start;

/**
 * Representation of a template file.
 */
class Template
{
    protected array $params = [];
    protected array $sections = [];

    protected ?string $layout;
    protected ?string $section;

    public function __construct(
        protected Engine $engine,
        protected string $name
    ) {}

    /**
     * Add single param to the internal params array.
     */
    public function addParam(string $name, mixed $param): void {
        $this->params[$name] = $param;
    }

    /**
     * Add params to the internal params array.
     */
    public function addParams(array $params): void {
        $this->params = array_merge($this->params, $params);
    }

    /**
     * Renders a template file and returns the result as a string.
     */
    public function render(): string
    {
        ob_start();
        $this->includeFile($this->name, $this->params);
        $content = ob_get_clean();

        if (isset($this->layout)) {
            $layout = $this->engine->template($this->layout);
            
            $layout->sections = array_merge($this->sections, [
                'content' => $content
            ]);
            
            $layout->addParams($this->params);
            return $layout->render();
        }

        return $content;
    }

    /**
     * Attempt to load a template file.
     */
    protected function includeFile(string $file, array $params = []): void
    {
        // Unpack params array into vars
        extract($params);

        $filePath = $this->engine->resolveTemplatePath($file);

        if (file_exists($filePath)) {
            include $filePath;
        } else {
            throw new RuntimeException('Template file '.$filePath.' not found.');
        }
    }

    /**
     * Includes a template file into this template file.
     */
    protected function insert(string $file, array $params = []): void
    {
        // Merge insert params with template params.
        $mergedParams = array_replace($this->params, $params);

        $this->includeFile($file, $mergedParams);
    }

    /**
     * Define the template's layout.
     */
    public function layout(string $name): void {
        $this->layout = $name;
    }

    /**
     * Start a section.
     */
    public function start(string $name): void
    {
        if ($name === 'content') {
            throw new LogicException(
                'The section name content is reserved.'
            );
        }

        if (!empty($this->section)) {
            throw new LogicException(
                'You cannot nest sections within other sections.'
            );
        }

        $this->section = $name;
        ob_start();
    }

    /**
     * End a section.
     */
    public function end(string $name): void
    {
        if (empty($this->section)) {
            throw new LogicException(
                'You must start a section before you can stop it.'
            );
        }

        $this->sections[$this->section] = ob_get_clean();
        $this->section = null;
    }

    /**
     * Retrieve section content.
     */
    public function section(string $name): void
    {
        if (array_key_exists($name, $this->sections)) {
            echo $this->sections[$name];
        }
    }
}
