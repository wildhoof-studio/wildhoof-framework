<?php

declare(strict_types=1);

namespace Wildhoof\View;

/**
 * Template Engine Configuration.
 */
class Config
{
    public function __construct(
        private readonly string $directory,
        private readonly string $fileExtension
    ) {}

    /**
     * Returns the directory configuration.
     */
    public function getDirectory(): string {
        return $this->directory;
    }

    /**
     * Returns the file extension configuration.
     */
    public function getFileExtension(): string {
        return $this->fileExtension;
    }
}
