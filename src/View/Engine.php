<?php

declare(strict_types=1);

namespace Wildhoof\View;

use function ltrim;
use function rtrim;

/**
 * Template Engine for rendering HTML templates in raw PHP.
 */
class Engine
{
    public const DIRECTORY_SEPARATOR = '/';
    public const FILE_EXTENSION_SEPARATOR = '.';

    protected string $directory;
    protected string $fileExtension;

    public function __construct(Config $config)
    {
        $this->setDirectory($config->getDirectory());
        $this->setFileExtension($config->getFileExtension());
    }

    /**
     * Sets the cleaned directory without a trailing directory separator.
     */
    public function setDirectory(string $directory): void
    {
        // Remove directory separator to avoid double slashes.
        $this->directory = rtrim(
            $directory,
            self::DIRECTORY_SEPARATOR
        );
    }

    /**
     * Sets the cleaned filed extension without a starting dot.
     */
    public function setFileExtension(string $fileExtension): void
    {
        // Remove file extension separator to avoid duplication.
        $this->fileExtension = ltrim(
            $fileExtension,
            self::FILE_EXTENSION_SEPARATOR
        );
    }

    /**
     * Combines the directory, name and file extension to a full template path.
     */
    public function resolveTemplatePath(string $name): string
    {
        // Build directory path and extension.
        $directory = $this->directory . self::DIRECTORY_SEPARATOR;
        $extension = self::FILE_EXTENSION_SEPARATOR . $this->fileExtension;

        // Return full file path.
        return $directory . $name . $extension;
    }

    /**
     * Renders a template file and returns the result as a string
     */
    public function template(string $name): Template {
        return new Template($this, $name);
    }
}
