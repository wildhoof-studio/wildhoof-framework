<?php

declare(strict_types=1);

namespace Wildhoof\Database;

use function sprintf;

/**
 * Class containing the database config.
 */
class Config
{
    private const DSN_SCHEMA = 'mysql:host=%s;dbname=%s;charset=%s;';

    private string $username;
    private string $password;

    private string $host;
    private string $name;
    private string $charset;

    /**
     * Set the database username and password.
     */
    public function setUser(string $username, string $password): void
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Set the database info.
     */
    public function setDatabase(string $host, string $name, string $charset): void
    {
        $this->host = $host;
        $this->name = $name;
        $this->charset = $charset;
    }

    /**
     * Get the dsn string.
     */
    public function getDsn(): string
    {
        return sprintf(
            self::DSN_SCHEMA,
            $this->host, $this->name, $this->charset
        );
    }

    /**
     * Get the username.
     */
    public function getUsername(): string {
        return $this->username;
    }

    /**
     * Get the password.
     */
    public function getPassword(): string {
        return $this->password;
    }
}
