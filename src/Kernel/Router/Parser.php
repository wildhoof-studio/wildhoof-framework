<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Router;

use Wildhoof\Kernel\Http\Message\ServerRequest as Request;

use function count;
use function explode;
use function ltrim;
use function rtrim;
use function str_contains;
use function trim;

/**
 * Parses a request target as specified in the route pattern.
 */
class Parser
{
    /**
     * Splits a path in segments and returns them as an array.
     */
    private function splitPath(string $path): array
    {
        $cleaned = trim($path, '/');
        return explode('/', $cleaned);
    }

    /**
     * Checks if a segment is a param
     */
    private function isParam(string $segment): bool
    {
        $pos = strpos($segment, '{');
        return $pos !== false && $pos == 0;
    }

    /**
     * Gets the param name from the full param string.
     */
    private function getParamName(string $param): string
    {
        $param = ltrim($param, '{');
        $param = rtrim($param, '}');
        return explode(':', $param)[0];
    }

    /**
     * Puts the translated uri params in an array.
     */
    private function translate(array $path, array $pattern): array
    {
        $params = [];

        // Determine the number of segments
        $segments = count($pattern);

        // Loop through the pattern segments and find the params
        for ($i = 0; $i < $segments; $i++)
        {
            if ($this->isParam($pattern[$i]))
            {
                $paramName = $this->getParamName($pattern[$i]);
                $params[$paramName] = $path[$i];
            }
        }

        return $params;
    }

    /**
     * Attempts to parse a route request and stores the route paramenters in
     * the query params array of the server request.
     */
    public function parse(Request $request, Route $route): Request
    {
        $pattern = $route->getPattern();

        // If no parameter is present, respond with the original request
        if (!str_contains($pattern, '/{')) {
            return $request;
        }

        // Get the path and pattern segments
        $pathSegments = $this->splitPath($request->getRequestTarget());
        $patternSegments = $this->splitPath($pattern);

        // Get the params
        $params = $this->translate($pathSegments, $patternSegments);

        return $request->withQueryParams($params);
    }
}
