<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Router;

use function preg_match;
use function preg_replace;

/**
 * Class to determine if a Request matches a Request.
 */
class Matcher
{
    private array $tokens = [];

    private readonly string $placeholder;
    private readonly string $delimiter;
    private readonly string $default;

    public function __construct(Config $config)
    {
        $this->placeholder = $config->getPlaceholder();
        $this->delimiter = $config->getDelimiter();
        $this->default = $config->getDefault();
    }

    /**
     * Add a placeholder token and its regular expression to the token array.
     */
    public function addToken(string $name, string $regex): void {
        $this->tokens[$name] = $regex;
    }

    /**
     * Return the list of tokens with delimiters and a default expression.
     */
    public function getTokenList(): array
    {
        $tokens = [];

        // Add the token delimiter to the tokens first
        foreach ($this->tokens as $token => $expression) {
            $tokens[$this->delimiter . $token] = $expression;
        }

        // Add the default to the token list
        $tokens[''] = $this->default;

        return $tokens;
    }

    /**
     * Checks if the Route pattern matches the Request URI.
     */
    public function match(string $pattern, string $uri): bool
    {
        // Translate all tokens to regular expressions
        foreach ($this->getTokenList() as $token => $expression)
        {
            // Pattern for placeholders like {id:num}
            $search = '#[\{]' . $this->placeholder . $token . '[\}]#';

            $pattern = preg_replace($search, $expression, $pattern);
        }

        return (bool) preg_match('#^'.$pattern.'$#D', $uri);
    }
}
