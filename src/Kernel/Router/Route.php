<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Router;

use Wildhoof\Kernel\Http\Message\Method;
use Wildhoof\Kernel\Http\Message\Request;

use Wildhoof\Kernel\Http\Server\RequestHandlerInterface as Handler;

use InvalidArgumentException;

use function class_exists;
use function class_implements;
use function in_array;
use function sprintf;
use function trim;

/**
 * Represents the definition of a HTTP Route.
 */
class Route
{
    private readonly string $pattern;
    private readonly Method $method;
    private readonly string $handler;
    
    private array $middleware = [];

    public function __construct(string $pattern, Method $method, string $handler)
    {
        $this->setPattern($pattern);
        $this->method = $method;
        $this->setHandler($handler);
    }

    /**
     * Set pattern and normalize it, so that it begins with a slash and ends
     * without a trailing slash.
     */
    protected function setPattern(string $pattern): void {
        $this->pattern = '/'. trim($pattern, '/');
    }

    /**
     * Returns the normalized route pattern.
     */
    public function getPattern(): string {
        return $this->pattern;
    }

    /**
     * Uses the provided Matcher to check whether this Route matches the
     * provided Request and returns the result as a boolean.
     */
    public function matchesRequest(Matcher $matcher, Request $request): bool
    {
        // First check the Request method
        if ($this->method != $request->getMethod()) {
            return false;
        }

        // Finally check the pattern using the provided Matcher
        return $matcher->match($this->pattern, $request->getRequestTarget());
    }
    
    /**
     * Sets the route request handler.
     */
    public function setHandler(string $handler): void
    {
        if (!class_exists($handler)) {
            $message = sprintf('Request handler %s does not exist!', $handler);
            throw new InvalidArgumentException($message);
        }
        
        if (!in_array(Handler::class, class_implements($handler))) {
            $message = 'Handler must be an implementation of';
            $message .= ' RequestHandlerInterface!';
            throw new InvalidArgumentException($message);
        }
        
        $this->handler = $handler;
    }

    /**
     * Return the route request handler.
     */
    public function getHandler(): string {
        return $this->handler;
    }
    
    /**
     * Adds an array of middleware that will be run before the call of the
     * action handler.
     */
    public function addMiddleware(array $middleware): void {
        $this->middleware = $middleware;
    }

    /**
     * Returns the route middleware.
     */
    public function getMiddleware(): array {
        return $this->middleware;
    }
}
