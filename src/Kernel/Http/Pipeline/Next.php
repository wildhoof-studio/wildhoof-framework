<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Http\Pipeline;

use Wildhoof\Kernel\Http\Server\RequestHandlerInterface as Handler;

use Wildhoof\Kernel\Http\Message\ServerRequest as Request;
use Wildhoof\Kernel\Http\Message\Response;

/**
 * Handles the next middleware in the pipeline.
 */
class Next implements Handler
{
    private Queue $queue;
    private Handler $handler;

    public function __construct(Queue $queue, Handler $handler)
    {
        $this->queue = clone $queue;
        $this->handler = $handler;
    }

    /**
     * Handles a request and produces a response.
     */
    public function handle(Request $request): Response
    {
        $middleware = $this->queue->getFromQueue();

        if (!is_null($middleware)) {
            return $middleware->process($request, $this);
        }

        return $this->handler->handle($request);
    }
}
