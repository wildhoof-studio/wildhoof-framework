<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Http\Pipeline;

use Wildhoof\Kernel\Http\Server\MiddlewareInterface;

use function array_shift;

/**
 * A queue of middleware to be executed.
 */
class Queue
{
    private array $middleware = [];

    /**
     * Add a middleware item to the end of the queue.
     */
    public function addToQueue(MiddlewareInterface $middleware): void {
        $this->middleware[] = $middleware;
    }

    /**
     * Get the next item in the middleware queue.
     */
    public function getFromQueue(): ?MiddlewareInterface {
        return array_shift($this->middleware);
    }
}
