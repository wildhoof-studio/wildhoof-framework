<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Http\Factory;

use Wildhoof\Kernel\Http\Message\MimeType;
use Wildhoof\Kernel\Http\Message\Response;
use Wildhoof\Kernel\Http\Message\Stream;

/**
 * Factory class for producing JSON Responses.
 */
final class JsonResponseFactory
{
    /**
     * Creates a new instance of Response with JSON type and given status code.
     */
    public function create(int $code, array $json): Response
    {
        $response = new Response($code);

        // Add Content Type JSON to headers.
        $mimeType = MimeType::JSON->value;
        $response = $response->withHeader('Content-Type', $mimeType);

        // Add json as converted stream to body
        $stream = new Stream(json_encode($json));
        return $response->withBody($stream);
    }
}
