<?php

namespace Wildhoof\Kernel\Http\Server;

use Wildhoof\Kernel\Http\Message\ServerRequest as Request;
use Wildhoof\Kernel\Http\Message\Response;

/**
 * Base request handler interface.
 */
interface RequestHandlerInterface
{
    /**
     * Handles a request and produces a response.
     */
    public function handle(Request $request): Response;
}
