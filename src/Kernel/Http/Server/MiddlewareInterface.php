<?php

namespace Wildhoof\Kernel\Http\Server;

use Wildhoof\Kernel\Http\Server\RequestHandlerInterface as Handler;

use Wildhoof\Kernel\Http\Message\ServerRequest as Request;
use Wildhoof\Kernel\Http\Message\Response;

/**
 * Middleware interface which is a parent to all other middleware classes.
 */
interface MiddlewareInterface
{
    /**
     * Processes the http request or delegates it to the handler.
     */
    public function process(Request $request, Handler $handler): Response;
}
