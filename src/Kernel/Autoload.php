<?php

declare(strict_types=1);

namespace Wildhoof\Kernel;

use function file_exists;
use function spl_autoload_register;
use function str_replace;

/**
 * Flexible autoload class.
 */
class Autoload
{
    public function __construct(
        private readonly string $root,
        private readonly string $extension = '.php'
    ) {}

    /**
     * Autoload function.
     */
    public function autoload(): void
    {
        spl_autoload_register(function (string $classname)
        {
            $classpath = str_replace('\\', '/', $classname);
            $path = $this->root . $classpath . $this->extension;

            if (file_exists($path)) {
                require $path;
            }
        });
    }
}
