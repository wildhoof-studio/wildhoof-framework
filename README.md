
## Wildhoof Framework

### Kernel

Kernel Package with
* Dependency Injection
    * Interfaces
    * Singletons
    * Arguments
    * Autoresolve
* Simple Event Handler with priorities
* HTTP Messages
    * Requests and Responses
    * Streams
* HTTP Pipelines
* HTTP Server Interfaces for
    * Middleware
    * Handlers
* HTTP Response Factories
* Router with
    * Route parameters
    * Route middleware
    * Not Found Route

### Database

Database Package with
* Query Builder with
    * Select
    * Insert
    * Update
    * Delete
* Database Wrapper
* Very simple ORM

### View

View Package with
* Template Engine with
    * Layouts
    * Sections
    * Inserts

### Requirements

* PHP 8.2
* PDO
* MySQL
